# ascii-warehouse-front

Assignment

In order to use it you have to install both project (ascii-warehouse-front & react-homework-master) (```npm install```).
Then lunch the server (from the server's root) thanks to:
```
npm start

```
and start the front app (from the front's root) thanks to:
```
yarn start
```


## Documentation
This web page is build over [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate), the very useful react componet [react-infinite-scroller](https://github.com/hourliert/react-infinite-scroller), [react Material design Light](https://github.com/react-mdl/react-mdl) and the library recommended by Facebook: [Immutable.JS](https://facebook.github.io/immutable-js/)


According to our specifications

> ___Discount Ascii Warehouse___
>
> This is an ecommerce site, where you can buy all sorts of ascii faces like (ノ・∀・)ノ > and ¯_(ツ)_/¯, in a wide variety of font sizes. The homepage should display a list of > products for people to browse. You should code this using react and redux.
>
> Please read the instructions and FAQ below before beginning.
> - products are displayed in a grid.
> - give the user an option to sort the products in ascending order. Can sort by > "size", "price" or "id".
> - each product has :
>   - a "size" field, which is the font-size (in pixels). We should display the faces > in their correct size, to give customers a realistic impression of what they're > buying.
>   - a "price" field, in cents. This should be formatted as dollars like `$3.51`.
>   - a "date" field, which is the date the product was added to the catalog. Dates > should be displayed in relative time (eg. "3 days ago") unless they are older than > 1 week, in which case the full date should be displayed.
> - the product grid should automatically load more items as you scroll down.
> - our product database is under high load due to growing demand for ascii, so please > display an animated "loading..." message while the user waits.
> - to improve the user's experience, we should always pre-emptively fetch the next > batch of results in advance, making use of idle-time.  But they still should not be > displayed until the user has scrolled to the bottom of the product grid.
> - when the user reaches the end and there are no more products to display, show the > message "~ end of catalogue ~".

>  _Ads features_

> - after every 20 products we need to insert an advertisement from one of our > sponsors. Use the same markup as the advertisement in the header, but make sure the > `?r` query param is randomly generated each time an ad is displayed.
> - Ads should be randomly selected, but a user must never see the same ad twice in a > row. (TODO)

> [react-homework](https://github.com/FoodMeUp/react-homework)


**Furthermore**

* Aims at being fully reactive and responsive (using react material design light's grid)
* We pre-emptively fetch data and add it to the infinite scroller
* We use the official recommended material design's colors.

## Warning
* We add a CORS handler to the back-end even if we weren't supposed to change it. But we couldn't find a quick way to add a proxy on the same port within our framework so we choose the fastest solution to an issue which doesn't exist in producion.

## Futur improvements
* We have just achieved some functionnal test, we must improve them using gunit for example.
* Enhance the design of the web page. (we could display a sort action element in a overlay and highlight which sort is done).
* Add some new features like an export one (almost already done)
* Enhance the UX (regarding the new features).
* Translate the error when the remote data isn't available or in the right dataType.
* Given that each ascii's face hasn't the same size, we could create a variable that will sum the size of the data fetched. If this sum isn't bigger that an threshold, we could ask straight some more data.
* We could add a copy feature that will allow the use to copy/paste the face (really quick to do).
* We should have a look to the issue higlighted by the warning and the fact that some "dates from now" aren't well sorted (issue with date's format).
* We will have to take care of duplicated ads in a row.
* Enhance the accessibility thanks to ARIA.

### Naming convention

> Many programs have variables that contain computed values: totals, averages, maximums, and so on. If you modify a name with a qualifier like Total, Sum, Average, Max, > Min, Record, String, or Pointer, put *the modifier at the end of the name*.
> ...
> This practice offers several advantages. First, *the most significant part of the variable name*, the part that gives the variable most of its meaning, is at the *front* , so it’s most prominent and gets read first. Second, by establishing this convention, you avoid the confusion you might create if you were to use both totalRevenue and revenueTotal in the same program. The names are semantically equivalent, and the convention would pre-vent their being used as if they were different. Third, a set of names like revenueTotal, expenseTotal, revenueAverage, and expenseAverage has a pleasing symmetry.

> [Code Complete 2 by Steve McConnell](http://www.stevemcconnell.com/cc.htm)


### Notes ###
* We could build most of those components as totally pure ones, but in a debug POV, we aren't doing it yet in order to have logs. (TODO).
* We try as much as possible to factorizee code. For example, each row's component is a wrapping of a basing element. This is why we could quickly change the whole display of our application.
