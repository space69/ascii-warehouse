import { connect } from 'react-redux';
import React, { PropTypes } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Spinner, IconButton } from 'react-mdl';
import cx from 'classnames';
import { List, fromJS } from 'immutable';
import s from './HomePageContainer.css';
import {
  fetchProducts
} from '../../core/actions/APIActions';
import {
  destockStorehouse,
  sortProducts
} from '../../core/actions/userActions';
import Header from '../../components/Header';
import WrappedItem from '../../components/WrappedItem';
import ProductHead from '../../components/ProductHead';


// Centralized config
const cfg = require('../../config.json');
const SORT = cfg.SORT_BY;
const SIZE_CHUNK = cfg.SIZE_CHUNK;
const SERVER_AD = cfg.SERVER_PRODUCTS_DEPLOYED;

// We foresee the need to load more data asap
const THRESHOLD_LOAD = 300; // The distance in pixels before the end of the items that will trigger a call to loadMore.

// Could be into another comp
const loaderComp = <div key={'loader'} className={cx(s.loader)}> <Spinner /><h4> Loading ...</h4></div>;

// Filter consts could be externalize (TODO)

// Basic function that compare 2 strings
function alphabetical(a, b) {
  return a.localeCompare(b);
}

const listToProduct = l => l.toJS()[0];

// l2 is first since we are using descending sort
const comparatorId = (l2, l1) => alphabetical(listToProduct(l1).id, listToProduct(l2).id);
const comparatorDate = (l2, l1) => new Date(listToProduct(l2).date).getTime() - new Date(listToProduct(l1).date).getTime(); // TODO we must find a proper way to sort date
const comparatorPrice = (l2, l1) => listToProduct(l2).price - listToProduct(l1).price;

const idHeadGen = dispatch => <div onClick={() => dispatch(sortProducts(comparatorId, 'id'))}>Id △</div>;
const dateHeadGen = dispatch => <div onClick={() => dispatch(sortProducts(comparatorDate, 'date'))}>Date △</div>;
const priceHeadGen = dispatch => <div onClick={() => dispatch(sortProducts(comparatorPrice, 'price'))}>Price △</div>;

const faceHead = <div>Face</div>;
let toProductHeadList = null;
class HomePageContainer extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(fetchProducts(0, 4 * SIZE_CHUNK, SORT));
    // We need to inject the dispatch only available within React
    toProductHeadList = fromJS({ idHead: idHeadGen(dispatch),
      faceHead,
      dateHead: dateHeadGen(dispatch),
      priceHead: priceHeadGen(dispatch) });
  }

  componentWillUpdate() {
    const { hasMoreStock, dispatch } = this.props;
    if (!hasMoreStock) { // It is better to have something into the storeHouse (foresee the need)
      dispatch(fetchProducts(0, 4 * SIZE_CHUNK, SORT));
    }
  }

  render() {
    const {
        onLoadMore,
        chunks,
        skip,
        limit,
        hasToUpdate,
        nbProductsDestocked
    } = this.props;
    if (typeof chunks !== 'undefined') {
      return (
        <div key={'main'}>
          <Header />
          <div key={'body'} className={cx(s.body)} >
            <ProductHead key={'head'} toProductHeadList={toProductHeadList} />
            <InfiniteScroll
              key={'infiniteScroll'}
              pageStart={0}
              loadMore={() => onLoadMore(nbProductsDestocked, SIZE_CHUNK, SORT)}
              hasMore={hasToUpdate}
              loader={loaderComp}
              useWindow
              threshold={THRESHOLD_LOAD}
            >
              {chunks.map((chunk, index) => {
                const comp = <WrappedItem key={chunk.get(0).id} isAd={index % SIZE_CHUNK === 0} chunk={chunk} />;
                if (index !== 0 && index % SIZE_CHUNK === 0) {
                  return [<div className={cx(s.add)}><img alt={`add ${index}`} src={`${SERVER_AD}ad/?r=${Math.floor(Math.random() * 1000)}`} /></div>, comp];
                }
                return comp;
              })}
            </InfiniteScroll>
            {loaderComp}
          </div>
          <div key={'footer'} className={cx(s.footer)}>
            The biggest ascii shop worldwide. Always ASk If we can help. Copyright 2017
          </div>
        </div>
      );
    }

    return <p>State wrong, please ask your beloved developer</p>;
  }

}


const mapStateToProps = state => ({
  chunks: state.current.chunks,
  hasToUpdate: state.current.hasToUpdate,
  skip: state.current.skip,
  limit: state.current.limit,
  products: state.current.products,
  nbProductsDestocked: state.current.nbProductsDestocked,
  hasMoreStock: state.current.hasMoreStock
});

const mapDispatchToProps = dispatch => ({
  onLoadMore: (skip, limit, sort) => {
    dispatch(destockStorehouse(skip, limit, sort));
    dispatch(fetchProducts(skip, limit, sort));
  },
  dispatch
});

const BoundHomePageContainer = connect(mapStateToProps, mapDispatchToProps)(HomePageContainer);

export default BoundHomePageContainer;
