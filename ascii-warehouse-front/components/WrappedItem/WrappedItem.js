
import React, { PropTypes } from 'react';
import cx from 'classnames';
import { List } from 'immutable'; // Here do remind us we use Immutable
import Product from '../Product';


const ImmutablePropTypes = require('react-immutable-proptypes');

import PureRenderMixin from 'react-addons-pure-render-mixin';

// Since we event to have some pure "dumb" component, we wrap those document with some Immutable.JS compliant one.
// IT will serve the right component Products|Ads

class WrappedItem extends React.Component {

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }


  // No more need of it thanks to react-addons-pure-render-mixin

  // // Even if the sole pros of this component are the same, tihs.props isn't the same, so we define it by ourselves
  //   // We should have a look on using
  //
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.error('shouldComponentUpdate');
  //   return !this.props.chunk === nextProps.chunk;
  // }

  render() {
    const {
      chunk
    } = this.props;
    return <Product product={chunk.toJS()} />;
  }

}

WrappedItem.propTypes = {
  chunk: ImmutablePropTypes.list.isRequired
};
export default WrappedItem;
