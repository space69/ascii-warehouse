import React from 'react';
import cx from 'classnames';
const moment = require('moment');
import ProductBase from '../ProductBase';

import s from './Product.css';

const NB_DECIMAL = 2;
const WEEKS_MS = 7 * 24 * 60 * 60 * 1000;

class Product extends React.Component {
  // Should not be called since we only read the product's data
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.error(this.props.product[0].id === nextProps.product[0].id);
  //   return true;
  // }

  render() {
    const { product } = this.props;
    return (<ProductBase
      idContent={product[0].id}
      faceContent={product[0].face}
      dateContent={moment().subtract(1, 'weeks').diff(moment(product[0].date)) > 0 ? moment(product[0].date).fromNow() : product[0].date}
      priceContent={`$${product[0].price.toFixed(NB_DECIMAL)}`}
      size={product[0].size}
      className={cx(s.product)}
    />);
  }

}
export default Product;
