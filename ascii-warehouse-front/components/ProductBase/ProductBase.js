import React from 'react';
import { Cell, Grid } from 'react-mdl';
import cx from 'classnames';

class ProductBase extends React.Component {
  // Should not be called since we only read the product's data
  shouldComponentUpdate(nextProps, nextState) {
    // console.error('ProductBase shouldn\'t update');
    return true;
  }

  render() {
    const {
      idContent,
      faceContent,
      dateContent,
      priceContent,
      size,
      className
    } = this.props;
    return (<Grid className={className} key={idContent}>
      <Cell key={'id'} className={cx('id', 'cell')} col={3}>{idContent}</Cell>
      <Cell key={'face'} className={cx('face', 'cell')} col={3} style={typeof size !== 'undefined' ? { fontSize: size } : null}>{faceContent}</Cell>
      <Cell key={'date'} className={cx('date', 'cell')} col={3}>{dateContent}</Cell>
      <Cell key={'price'} className={cx('price', 'cell')} col={3}>{priceContent}</Cell>
    </Grid>);
  }

}
export default ProductBase;
