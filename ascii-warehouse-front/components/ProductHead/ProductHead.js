import React from 'react';
import cx from 'classnames';
import s from './ProductHead.css';
import ProductBase from '../ProductBase';
import { List } from 'immutable'; // Here do remind us we use Immutable
import PureRenderMixin from 'react-addons-pure-render-mixin';

// This component could be wrapped into an immutableJS compliant one bit we do it straight (TODO)

// Since we event to have some pure "dumb" component, we wrap those document with some Immutable.JS compliant one.
// IT will serve the right component Products|Ads


class ProductHead extends React.Component {

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  render() {
    const { toProductHeadList } = this.props;
    const { idHead, faceHead, dateHead, priceHead } = toProductHeadList.toJS();
    return (<ProductBase
      idContent={idHead}
      faceContent={faceHead}
      dateContent={dateHead}
      priceContent={priceHead}
      className={cx(s.productHead)}
    />);
  }
}
export default ProductHead;
