
export const DESTOCK_STOREHOUSE = 'DESTOCK_STOREHOUSE';

export function destockStorehouse(skip, limit, sort) {
  return {
    type: DESTOCK_STOREHOUSE,
    payload: {
      skip, limit, sort
    }
  };
}


export const SORT_REQUEST = 'SORT_REQUEST';

export function sortProducts(comparator, newSort) {
  return {
    type: SORT_REQUEST,
    payload: { comparator, newSort }
  };
}
