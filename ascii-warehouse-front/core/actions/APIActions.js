import 'whatwg-fetch';
import api from '../api';

// PArsing function that trenasform the text raw date into useful JSON plain object
const paseRawProduct = (rawData) => {
  if (rawData.length > 0) {
    const byProduct = rawData.split('\n');
    byProduct.pop(); // last new line provide an empty element
    return byProduct.map(rawproduct => JSON.parse(rawproduct));
  }
  console.warn('Receiving empty raw product');
};

export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';

function requestProducts(skip, limit, sort) {
  return {
    type: REQUEST_PRODUCTS,
    payload: { skip, limit, sort }
  };
}

export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';

function receiveProducts(skip, limit, sort, json) {
  return {
    type: RECEIVE_PRODUCTS,
    payload: {
      skip,
      limit,
      sort,
      data: json },
    meta: {
      receivedAt: Date.now()
    }
  };
}

export const ERROR_PRODUCTS = 'ERROR_PRODUCTS';

function errorProducts(skip, limit, sort, message) {
  console.error(ERROR_PRODUCTS);
  console.error(message);

  return {
    type: ERROR_PRODUCTS,
    payload: {
      skip, limit, sort, message
    }
  };
}
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';

export function fetchProducts(skip, limit, sort) {
  return (dispatch) => {
    dispatch(requestProducts(skip, limit, sort));
    return fetch(api.products(skip, limit, sort))
                .then(
                  (response) => {
                    switch (response.status) {
                      case 200:
                        return response.text().then(
                          rawProducts => dispatch(receiveProducts(skip, limit, sort, paseRawProduct(rawProducts)))
                        );
                      default:
                        return dispatch(errorProducts(skip, limit, sort, status));
                    }
                  },
                    error => dispatch(errorProducts(skip, limit, sort, error))
                  );
  };
}
