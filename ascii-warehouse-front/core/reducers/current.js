import { List } from 'immutable';

import {
  REQUEST_PRODUCTS,
  RECEIVE_PRODUCTS,
  ERROR_PRODUCTS
} from '../actions/APIActions';

import {
  DESTOCK_STOREHOUSE,
  SORT_REQUEST
} from '../actions/userActions';

/*
*This element handle the curent data such as the current page, deviceSelected
*/

const buildChunksList = (receivedProducts) => {
  const nbReceived = receivedProducts.length;
  let newProductsChunks = List();
  let i = 0;
  while (i < nbReceived) {
    // We use slice givent that it does a swallow copy (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/slice)
    // We could as well use fromJS
    newProductsChunks = newProductsChunks.push(List(receivedProducts.slice(i, i + 1)));
    i += 1;
  }
  return newProductsChunks;
};
// Before sorting
// const destockStorehouse = (toLoad, storeHouse) => {
//   const updatedChunks = toLoad.concat(storeHouse);
//   return updatedChunks;
// };

const destockStorehouse = (toLoad, storeHouse, isSorted, sort, comparator) => {
  if (typeof (storeHouse) === 'undefined' || storeHouse.size === 0) {
    return toLoad;
  }
  if (!isSorted) {
    return toLoad.concat(storeHouse);
  }
  let toLoadDym = toLoad;
  let indexStore = 0;
  let indexToLoad = 0;
  const storeHouseSorted = storeHouse.sort(comparator);

  // We use the fact that the store is sorted. We just have one parcour
  while (indexStore < storeHouseSorted.size && indexToLoad < toLoadDym.size) {
    // if (storeHouseSorted.get(indexStore).get(0)[sort] <= toLoadDym.get(indexToLoad).get(0)[sort]) {
    if (comparator(storeHouseSorted.get(indexStore), toLoadDym.get(indexToLoad)) <= 0) {
      // If the first one need to be inserted
      // console.log(`${storeHouseSorted.get(indexStore).get(0)[sort]} <= ${toLoadDym.get(indexToLoad).get(0)[sort]}  ${indexStore}->${indexToLoad}`);
      toLoadDym = toLoadDym.insert(indexToLoad, storeHouseSorted.get(indexStore));
      indexStore += 1;
    }
    indexToLoad += 1;
  }
  // console.warn(`${indexStore} < ${storeHouseSorted.size} && ${indexToLoad} < ${toLoadDym.size}`);

  // We left without finishing to fully parcour the store
  if (indexStore < storeHouseSorted.size) {
    return toLoadDym.concat(storeHouseSorted.skip(indexStore + 1));
  }

  return toLoadDym;
};

// This state is the basic state that describes what is currently asked by the use

export default function current(
  state = {
    chunks: List(),
    hasToUpdate: false,
    needMoreStock: true,
    storeHouse: List(),
    hasMoreStock: false, // OFC, we could only use storeHouse.isEmpty(), but it is more clear this way and no need to use Immutable.JS into other component
    nbAds: 0,
    nbProductsDestocked: 0,
    skip: 0,
    limit: 0,
    isSorted: false,
    meta: {
      fetch: null,
      error: null,
      lastestReceivedAt: null // Date JS Object
    }
  },
  action) {
  switch (action.type) {
    case REQUEST_PRODUCTS:
      const requestedSkip = action.payload.skip;
      const requestedLimit = action.payload.limit;
      const requestedSort = action.payload.sort;

      return Object.assign({}, state, {
        meta: {
          fetch: true,
          requestedSkip,
          requestedLimit,
          requestedSort
        },
        hasToUpdate: false
      });
    case RECEIVE_PRODUCTS:
      const receivedSkip = action.payload.skip;
      const receivedLimit = action.payload.limit;
      // const receivedSort = action.payload.sort;
      const receivedProducts = action.payload.data;

      // Receivec nothing
      if (receivedProducts.length === 0) {
        console.warn('nothing fetched');
        return state;
      }
      if (!state.needMoreStock) {
        // We stock the new chunks
        return Object.assign({}, state, {
          meta: {
            fetch: false
          },
          lastestReceivedAt: action.meta.receivedAt,
          skip: receivedSkip,
          limit: receivedLimit,
          storeHouse: buildChunksList(receivedProducts),
          hasMoreStock: true
        });
      }
      // We destock and push straight the data received
      const storeHouseAndNewChunks = state.storeHouse.concat(buildChunksList(receivedProducts));
      return Object.assign({}, state, {
        meta: {
          fetch: false
        },
        lastestReceivedAt: action.meta.receivedAt,
        skip: receivedSkip,
        limit: receivedLimit,
        nbProductsDestocked: state.nbProductsDestocked + storeHouseAndNewChunks.size,
        chunks: destockStorehouse(state.chunks, storeHouseAndNewChunks, state.isSorted, state.sort, state.comparator),
        storeHouse: List(),
        hasMoreStock: false,
        hasToUpdate: true,
        needMoreStock: false
      });


    case ERROR_PRODUCTS:
      return Object.assign({}, state, {
        meta: {
          fetch: false,
          error: true
        },
        hasToUpdate: true
      });
    case DESTOCK_STOREHOUSE:

      if (!state.storeHouse.isEmpty()) {
        return Object.assign({}, state, {
          nbProductsDestocked: state.nbProductsDestocked + state.storeHouse.size,
          chunks: destockStorehouse(state.chunks, state.storeHouse, state.isSorted, state.sort, state.comparator),
          storeHouse: List(),
          hasMoreStock: false,
          hasToUpdate: true,
          needMoreStock: false
        });
      }
      // No chunk in storeHouse but still need new chunks
      return Object.assign({}, state, {
        hasToUpdate: false,
        hasMoreStock: false,
        needMoreStock: true
      });
    case SORT_REQUEST:
      const newSort = action.payload.newSort;
      const comparator = action.payload.comparator;
      if (typeof comparator === 'function') {
        return Object.assign({}, state, {
          chunks: state.chunks.sort(comparator),
          isSorted: true,
          sort: newSort,
          comparator
        });
      }
      console.error(`${comparator} isn't a function, aborting`);
      return state;
    default:
      return state;
  }
}
