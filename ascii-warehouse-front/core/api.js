const cfg = require('../config.json');

const SERVER_PRODUCTS = cfg.SERVER_PRODUCTS_DEPLOYED;


// Build the GET query parameters from an objectect {key:value,...}
const buildParams = (paramsObj) => {
  const arrayKeysParams = Object.keys(paramsObj);
  if (arrayKeysParams.length > 0) {
    let ret = '?';
    arrayKeysParams.forEach((key) => {
      if (typeof paramsObj[key] !== 'undefined') {
        ret += `&${key}=${paramsObj[key]}`;
      }
    });
    return ret;
  }
  return '';
};

// Provide the appropriate URL to fetch in order to GET more products
const products = (skip, limit, sort) => `${SERVER_PRODUCTS}api/products${buildParams({ skip, limit, sort })}`;
export default { products };
